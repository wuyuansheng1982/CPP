// CommandChain.cpp : This file contains the 'main' function. Program execution begins and ends there.
// https://helloacm.com/c-object-method-chaining/

#include <iostream>

class Calc
{
private:
    int m_value = 0;

public:
    Calc(int value = 0) : m_value(value) {}

    Calc& add(int value) { m_value += value;  return *this; }
    Calc& sub(int value) { m_value -= value;  return *this; }
    Calc& mult(int value) { m_value *= value;  return *this; }

    int getValue() { return m_value; }
};

int main()
{
    Calc calc{};
    calc.add(5).sub(3).mult(4);

    std::cout << calc.getValue() << '\n';
    return 0;
}

