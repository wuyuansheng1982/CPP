#include <iostream>

using namespace std;

int main()
{
    int arr[5] = { 1,2,3,4,5 };
    cout << sizeof(arr) << "\tsize of array\n";
    cout << sizeof(arr[0]) << "\tsize of int\n";
    cout << sizeof(&arr) << "\tsize of pointer, depending of x86 or x64, 4 bytes or 8 bytes\n";

    cout << &arr << "\tarray starting address\n";
    cout << &arr + 1 << "\tarray + sizeof(array) address\n";
    cout << *(&arr + 1) << "\tconvert to pointer, same as (int *)(&arr +1)\n";
    cout << *(&arr + 1) - 1 << "\tminus 1 int size\n";
    cout << *(*(&arr + 1) - 1) << "\tdereference pirnt last element\n";
    return 0;
}
/*
https://stackoverflow.com/questions/19421971/pointer-to-a-reference-plus-one-of-array
*/
